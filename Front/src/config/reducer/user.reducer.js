/* eslint-disable import/no-anonymous-default-export */
import {EMAIL_USER, ADD_USER} from '../actions/user.action'

const initState = {
    emailUser: 'init email',
}

export default (state = initState, action) => {
    switch (action.type) {
        case EMAIL_USER:
            return ({
                ...state,
                emailUser: state.emailUser
            });
        case ADD_USER:
            return({...state });
        default:
            return state
    }
}