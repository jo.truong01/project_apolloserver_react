import React from 'react';

import {Provider} from 'react-redux'
import {store} from './config/store'
import RouterMenu from './components/Router'
import { ApolloClient, ApolloProvider, InMemoryCache } from '@apollo/client';
import 'bootstrap/dist/css/bootstrap.css'
import './css/style.css'
// like a provider (Redx)
const client = new ApolloClient({uri: "http://localhost:5000/graphql", cache: new InMemoryCache()})

function App() {
  return (
    <ApolloProvider client={client}>
      <Provider store={store}>
        <div className="App">
          <RouterMenu></RouterMenu>
        </div>
      </Provider>
    </ApolloProvider>
  );
}

export default App;
