import React, {useState} from 'react';
import {useHistory } from 'react-router-dom'
import Axios from 'axios'
import {Container, Form, Button} from 'react-bootstrap'

const LoginForm = () => {
    const [password, setPassword] = useState('')
    const [email, setEmail] = useState('')
    let history = useHistory()
  
    function handleMdp(event) {
      setPassword(event.target.value)
    }
  
    function handleEmail(event) {
      setEmail(event.target.value)
    }
  
    function handleLog() {
      const cred = { username: email, password: password }
      Axios.post('https://easy-login-api.herokuapp.com/users/login', cred)
        .then(resp => {
          localStorage.setItem('Token', resp.headers['x-access-token'])
          localStorage.setItem('Niveau', 3)
          history.push('/formation')
        })
        .catch(error => {
          console.log('ERROR: ', error)
        })
    }
  
    return (
      <Container>
        <Form>
            <Form.Group controlId="exampleForm.ControlInput1">
                <Form.Label>Email:</Form.Label>
                <Form.Control type="email" placeholder="prenom@exemple.com" onChange={handleEmail}/>
                <Form.Label>Mot de passe:</Form.Label>
                <Form.Control type="password" placeholder="Exemple: 1234" onChange={handleMdp}/>
                <Button variant="success" onClick={handleLog}>Connexion</Button>
            </Form.Group>    
        </Form>
      </Container>
    )
}

export default LoginForm