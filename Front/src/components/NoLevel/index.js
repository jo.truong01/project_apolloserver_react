import React from 'react';
import {Jumbotron} from 'react-bootstrap'

const NoLevel = () => {
    return (
        <div>
            <Jumbotron>
                <h3>Vous n'avez pas le niveau nécessaire pour effectuer cette action</h3>
            </Jumbotron>
        </div>
    );
};

export default NoLevel;