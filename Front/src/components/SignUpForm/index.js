import React, {useState} from 'react';

const SignUpForm = () => {

    const [formValue, setFormValue] = useState({})
    const handleSubmit = () =>{
        
    }
    const handleChange = (event) => {
        setFormValue({...formValue, [event.target.name]: event.target.value})
    }

    return (
        <div>
            <form>
                <label>Prénom:</label>
                <input type="text" onChange={handleChange}/>
                <label>Nom:</label>
                <input type="text" onChange={handleChange}/>
                <label>Email:</label>
                <input type="email" onChange={handleChange}/>
                <label>Téléphone:</label>
                <input type="phone" onChange={handleChange}/>
                <button onClick={handleSubmit}></button>
            </form>
        </div>
    );
};

export default SignUpForm;