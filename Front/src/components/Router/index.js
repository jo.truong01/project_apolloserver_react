import React from 'react'

import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'

import Menu from '../Menu'
import PrivateRoute from './privateRoute'

import Home from '../../pages/home'
import About from '../../pages/about'
import Formation from '../../pages/formation'
import Contact from '../../pages/contact'
import Login from '../../pages/login'
import NoPage from '../../pages/404'
import MyFormation from '../../pages/vos_formations'
import FormationModif from '../../pages/formation_modif'
import FormationCreate from '../../pages/formation_creation'
import Logout from '../../pages/logout'
import NoLog from '../../pages/no_log'


const RouterMenu = () => {
  return (
    <Router>
      <Menu></Menu>
      <Switch>
        <Route exact path='/' component={Home}/>
        <Route exact path='/formation' component={Formation}/>
        <Route exact path='/formation/:id' component={Formation}/>
        <Route exact path='/about' component={About}/>
        <Route exact path='/contact' component={Contact}/>
        <Route exact path='/login' component={Login}/>
        <Route exact path='/noLog' component={NoLog}/>
        <PrivateRoute path='/vos_formations' component={MyFormation}/>
        <PrivateRoute exact path='/formation/modif/:id' component={FormationModif}/>
        <PrivateRoute exact path='/create' component={FormationCreate}/>
        <PrivateRoute path='/logout' component={Logout}/>
        <Route path='*'>
          <NoPage></NoPage>
        </Route>
      </Switch>
    </Router>
  )
}

export default RouterMenu