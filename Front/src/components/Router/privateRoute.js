import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import PropTypes from 'prop-types'

const PrivateRoute = ({ component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={props => {
        return localStorage.getItem('Token') ? (
          <Component {...props} />
        ) : (
          <Redirect to='/noLog' />
        )
      }}
    ></Route>
  )
}

PrivateRoute.propTypes = {
  component: PropTypes.any
}

export default PrivateRoute
