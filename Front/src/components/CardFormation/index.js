import React from 'react';
import {Card, Button} from 'react-bootstrap'
import {Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import trainingImage from '../../assets/training.jpg'

const CardFormation = ({title, text, id, difficulty}) => {
    
    return (
    <Card>
        <Card.Img variant="top" src={trainingImage}/>
        <Card.Body>
            <Card.Title>{title}</Card.Title>
            <Card.Subtitle>Difficulté: {difficulty}</Card.Subtitle>
            <Card.Text>
                {text.slice(0, 25)}...
            </Card.Text>
        </Card.Body>
        <Card.Footer>
            <Link to={`/formation/${id}`} ><Button type="button" className="Primary">Vers la Formation</Button></Link>
        </Card.Footer>
  </Card>
    );
};

CardFormation.propTypes = {
    title: PropTypes.string,
    text: PropTypes.string,
    id: PropTypes.string,
    difficulty: PropTypes.number
}

export default CardFormation;