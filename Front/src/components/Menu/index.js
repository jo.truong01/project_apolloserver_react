import React, { useEffect} from 'react';
import {Nav, Container} from 'react-bootstrap'
import './menu.css'

const Menu = () => {
    const connected = localStorage.getItem('Token')

    useEffect(() => {
        console.log('connected => ', connected)
    }, [connected])

    return (
        <Container className='menu-navbar'>
            <Nav className="justify-content-center" activeKey="/home">
        <Nav.Item>
            <Nav.Link href="/">Accueil</Nav.Link>
        </Nav.Item>
        <Nav.Item>
            <Nav.Link href='/formation'>Formation</Nav.Link>
        </Nav.Item>
        <Nav.Item>
            <Nav.Link href='/about'>A propos</Nav.Link>
        </Nav.Item>
        {connected ?(
        <Nav.Item>
            <Nav.Link href='/vos_formations'>Page Perso</Nav.Link>
        </Nav.Item>
        ) : (
            <Nav></Nav>
        )}
        <Nav.Item>
            <Nav.Link  href='/contact'>Contact</Nav.Link>
        </Nav.Item>
        {connected ? (
            <Nav.Item>
                <Nav.Link  href='/logout'>Déconnexion</Nav.Link>
            </Nav.Item>
        ): (
            <Nav.Item>
            <Nav.Link  href='/login'>Connexion</Nav.Link>
        </Nav.Item>
        )}
    </Nav>
  </Container>
    );
};

export default Menu;