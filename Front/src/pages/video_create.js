import React, { useState } from 'react';
import {Container, Form, Row } from 'react-bootstrap'

const AdminPage = () => {

    const [desc, setDesc] = useState('')

    const handleDesc = (event) => {
        console.log('test =>', event.target)
        setDesc(event.target)
        console.log("desc =>", desc)
    }

    return (
        <Container>
            <h2>Video create Page:</h2>
            <Row>
                <Form>
                <legend>Ajout d'une vidéo: </legend>
                <Form.Group controlId="exampleForm.ControlInput1">
                    <Form.Label>Nom Formation:</Form.Label>
                    <Form.Control type='text' onChange/>
                    <Form.Label>Description:</Form.Label>
                    <textarea name="description" onChange={handleDesc}></textarea>
                </Form.Group>
                </Form>
            </Row>
        </Container>
    );
};

export default AdminPage;