import React from 'react'
import {Container} from 'react-bootstrap'
import { useLocation } from 'react-router-dom'

const NoPage = () => {
  let location = useLocation()
  return <Container>ERROR page not founed at : {location.pathname}</Container>
}

export default NoPage
