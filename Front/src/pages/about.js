import React from 'react';
import {Container, Tab, Tabs, Row} from 'react-bootstrap'
import '../css/aboutPage.css'

const About = () => {
    return (
        <Container className="about-content">
            <Row>
                <div>
                    <h2>Un petit résumé de l'histoire de la discipline et le créateur</h2>
                </div>
            </Row>
            <Tabs defaultActiveKey="about" id="uncontrolled-tab-example">
                <Tab eventKey='about' title="A propos">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                    Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                    It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                </Tab>
                <Tab eventKey='discipline' title="La discipline">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                </Tab>
                <Tab eventKey='history' title="Notre Histoire">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                    Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                    It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                    It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                </Tab>
            </Tabs>
        </Container>
    );
};

export default About;