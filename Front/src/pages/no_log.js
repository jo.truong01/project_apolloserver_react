import React from 'react';
import {Container, Row } from 'react-bootstrap'
import {Link} from 'react-router-dom'

const NoLog = () => {
    return (
        <Container>
            <Row className="justify-content-center">
                <h2>Vous n'êtes pas connecté pour ce genre d'action <Link to='/login'>Cliquez ici</Link></h2>
            </Row>
            
        </Container>
    );
};

export default NoLog;