import React, {useState, useEffect} from 'react';
import {Container, Form, Button, Col} from 'react-bootstrap'
import Axios from 'axios'
import {useParams} from 'react-router-dom'

const FormationModif = () => {

    let Niveau = localStorage.getItem('Niveau')
    const [NewForm, setNewForm] = useState({})
    const [OldForm, setOldForm] = useState({})

    let {id} = useParams()

    useEffect(() => {
        Axios.get(`http://localhost:5000/api/formation/${id}`)
        .then(result => {
            setOldForm(result.data)
        })
        .catch(err => {
            console.error("Error get modif formation : ", err)
        })
    }, [id])

    const handleChange = (event) => {
        const name = event.target.name
        const value = event.target.value
        setNewForm({...NewForm,[name]: value})
    }

    const handleSubmit = () => {
            Axios.post(`http://localhost:5000/api/formation/modif/${id}`,{id: id , modif: NewForm})
            .then(result => {
                console.log('Result POST =>', result)
            })
            .catch(err => {console.error("ERROR MODIF FORMATION =>", err)})
    } 

    return (
        <Container>
            {Niveau === '3' ? (
            <Col xs={8}>
                <Form>
                    <Form.Group>
                        <Form.Label>Titre Formation: </Form.Label>
                        <Form.Control type="text" onChange={handleChange} defaultValue={OldForm.name} name="name" />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Description:</Form.Label>
                        <Form.Control as='textarea' rows={5} onChange={handleChange} defaultValue={OldForm.description} name="description" />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Prix:</Form.Label>
                        <Form.Control type='number' onChange={handleChange} defaultValue={OldForm.price} name="price"/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Difficulté:</Form.Label>
                        <Form.Control as="select" multiple onChange={handleChange} defaultValue={OldForm.difficulty} name="difficulty">
                            <option value="1">Facile</option>
                            <option value="2">Moyen</option>
                            <option value="3">Difficile</option>
                        </Form.Control>
                    </Form.Group>
                    <Button variant="success" onClick={handleSubmit}>
                        Modifier
                    </Button>
                </Form>
            </Col>
            ) : (
                <Col xs={6}>
                    <h2>Vous n'avez pas le niveau nécessaire pour effectuer cette action</h2>
                </Col>
            )}
        </Container>
    );
};

export default FormationModif;