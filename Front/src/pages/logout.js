import React from 'react'
import { useHistory } from 'react-router-dom'
import {Container, Row, Button} from 'react-bootstrap'

const Logout = () => {
  let history = useHistory()

  const handleLogout = () => {
    localStorage.removeItem('Token')
    localStorage.removeItem('Niveau')
    history.push('/')
  }
  const handleNo = () => {
    history.push('/')
  }

  return (
    <Container>
        <Row className="justify-content-center">
            <h2>êtes vous sur de nous quitter ?</h2>
        </Row>
        <Row className="justify-content-center">
            <Button variant="success" onClick={handleLogout}>Oui</Button>
            <Button variant="danger" onClick={handleNo}>Non</Button>
        </Row>
    </Container>
  )
}

export default Logout
