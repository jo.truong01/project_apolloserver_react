import React, { useState } from 'react';
import {Container, Row, Form, Button, Col} from 'react-bootstrap'
import Axios from 'axios'
import NoLevel from '../components/NoLevel';
import {useHistory} from 'react-router-dom'

const FormationCreate = () => {
    const history = useHistory()
    let Niveau = localStorage.getItem('Niveau')
    const [formValue, setFormValue] = useState({})

    const handleChange = (event) => {
        const name = event.target.name
        const value = event.target.value
        setFormValue({...formValue,[name]: value})
        console.log('formValue =>', formValue)
    }

    const handleSubmit = () => {
        Axios.post('http://localhost:5000/api/formation/create', {formValue})
        .then(result => {
            console.log('result create =>', result)
            alert("Creation OK")
            history.push("/formation")
        })
        .catch(err => {console.error("ERROR Create Formation: ",err)})
    } 

    return (
        <Container>
            <h2>Page de création:</h2>
            <Row>
            {Niveau === '3' ? (
            <Col xs={8}>
                <Form>
                    <Form.Group>
                        <Form.Label>Titre Formation: </Form.Label>
                        <Form.Control type="text" onChange={handleChange} name="name" />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Description:</Form.Label>
                        <Form.Control as='textarea' rows={5} onChange={handleChange}  name="description" />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Prix:</Form.Label>
                        <Form.Control type='number' onChange={handleChange}  name="price"/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Difficulté:</Form.Label>
                        <Form.Control as="select" multiple onChange={handleChange}  name="difficulty">
                            <option value="1">Facile</option>
                            <option value="2">Moyen</option>
                            <option value="3">Difficile</option>
                        </Form.Control>
                    </Form.Group>
                    <Button variant="success" onClick={handleSubmit}>
                        Créer
                    </Button>
                </Form>
            </Col>
            ) : (
                <Col xs={6}>
                    <NoLevel/>
                </Col>
            )}
            </Row>
        </Container>
    );
};

export default FormationCreate;