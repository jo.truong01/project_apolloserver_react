import React, {useState, useEffect} from 'react';
import { Container, Row, CardGroup, Button, Jumbotron, Col} from 'react-bootstrap';
import CardFormation from '../components/CardFormation'
import Axios from 'axios'
import {useParams, Link} from 'react-router-dom'
import '../css/formationPage.css'

const Formation = () => {

    let {id} = useParams()
    let Niveau = localStorage.getItem('Niveau')
    const [AllFormation, setAllFormation] = useState([])
    const [ThisFormation, setFormation] = useState({})

    useEffect(() => {
        if (id)
        {
            Axios.get(`http://localhost:5000/api/formation/${id}`)
            .then(result => {
                setFormation(result.data)
            })
        }
        else {
            Axios.get('http://localhost:5000/api/Allformation')
            .then((result) => {
                setAllFormation(result.data)
            })
            .catch((err) => {console.error("ERROR FORMATION =>",err)})
        }
    }, [id])

    const ContentPage = (id) => {
        if (id)
        {
            return (
                <div>
                    <h3>Titre:</h3><h4>{ThisFormation.name}</h4>
                    <h3>Description:</h3><br/>
                    <p>
                        {ThisFormation.description}
                    </p>
                    <h3>Difficulté:</h3><p> {ThisFormation.difficulty}</p>
                    <h3>Prix:</h3><p> {ThisFormation.price}</p>
                    <Link to={`/formation/modif/${id}`}><Button variant='warning'>Modifier</Button></Link>
                </div>
            )
        }
        else {
            return (
                AllFormation.length > 0 ? (AllFormation.map((formation, index) => {
                    return (
                        <Col xs={4}>
                            <CardFormation title={formation.name} text={formation.description} difficulty={formation.difficulty} 
                             id={formation._id}/>
                        </Col>
                    )
                 })
                )
                  : (
                     <h2> Pas de formation dispo </h2>
                    )
            )
        }
    }

    return (
        <Container className="formation-content">
            <Row className='title-page justify-content-center'>
                <h2>Page Formation:</h2>
            </Row>
            <Jumbotron>
                {Niveau === '3' }
                        <Link to="/create"><Button type="button" className="btn-create">Créer votre Formation</Button></Link>
                <CardGroup>
                    {ContentPage(id)}
                </CardGroup>
            </Jumbotron>
        </Container>
    );
};

export default Formation;