// backend
import {apolloServer} from './apollo'
import {mongoService} from './service/mongoose.service'
const express = require("express")
const Router = require("./router")
const RouterVideo = require ('./router/routerVideo.js')
const cors = require('cors')
const bodyparser = require('body-parser')

const app = express()

app.use(cors())
app.use(bodyparser.json())
app.use(bodyparser.urlencoded({extended: true}))
app.listen({port: 5000}, () => {
    console.log('Server Express Connected on : http://localhost:5000')
})

app.use('/api', Router)
app.use('/video', RouterVideo)
app.get('/', function (req:any, res:any){
    
    res.send('Hello Server works properly go to /api')
})

mongoService.DBconnect()

apolloServer.applyMiddleware({app})