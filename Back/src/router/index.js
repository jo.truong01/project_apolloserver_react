var router = require('express').Router()

var Formation = require('../apollo/resolvers/formation.resolver')
var User = require('../apollo/resolvers/user.resolver')

var formationQuery = Formation.default.Query
var userQuery = User.default.Query
var formationMutation = Formation.default.Mutation

router.get('/test', function(req, res){
    res.setHeader('Content-Type', 'application/json')
    res.end(JSON.stringify({data: 'hello'}, null, 3))
})

router.get('/AllUser', (req, res) => {
    try{
        userQuery.getAllUser().then(result=>{
            res.status(200)
            .send(result)
        })
    }
    catch(err) {
        res.status(500)
        .send({error: err})
    }
})

router.post('/login', (res, req) => {
})

router.get('/AllFormation', (req, res) => {
    try{
        formationQuery.getAllFormation().then(result => {
            res.status(200)
            .send(result)
        })
    }
    catch(err){
        res.status(500)
        .send({error: err})
    }
})

router.get('/formation/:id', (req, res) => {
    try{
        formationQuery.getFormation(req.params.id)
        .then(result => {
            res.status(200)
            .send(result)
        })
    }
    catch(err) {
        res.status(500)
        res.send({error: err})
    }
})

router.post('/formation/create', (req, res) => {
    try{
        formationMutation.createFormation(req.body.formValue)
        .then(result => {
            res.status(200)
            .send({success: "OK", data: result})
        })
    }
    catch(err) {
        res.status(500)
        .send({error: err})
    }
})

router.post('/formation/modif/:id', (req, res) => {
    let body = req.body
    try{
        formationMutation.modifyFormation(id= body.id, modif= body.modif)
        .then(result =>{
            res.status(200)
            .send({success: "OK", data: result})
        })
    }
    catch(err){
        res.status(500)
        .send({error: err})
    }
})

router.get('/', function(req, res){
    res.send('api racine')
})

module.exports = router