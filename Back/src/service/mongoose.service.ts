import mongoose from 'mongoose'

 class MongoService {
     public urlDB : string

     constructor(url: string) {
         this.urlDB = url
     }

     public DBconnect(){
         mongoose.connect(this.urlDB, {useNewUrlParser: true, useUnifiedTopology: true})
         .then ( () => {
            return console.log('success connect to DB')
         },
         (err) => {
             console.error("ERROR DB : ", err)
         }
         )
     }
}

export const mongoService: MongoService = new MongoService("mongodb+srv://jo:1234J0@mydb01.dwpra.mongodb.net/Test?retryWrites=true&w=majority")