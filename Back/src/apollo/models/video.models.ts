import mongoose from 'mongoose'

interface IVideo extends mongoose.Document{
    name: string,
    path: string
}

const VideoModel = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    path: {
        type: String,
        required: true
    }
})

const Video = mongoose.model<IVideo>('Video', VideoModel)

export default Video