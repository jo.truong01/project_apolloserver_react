import { defaultTypeResolver } from 'graphql'
import mongoose from 'mongoose'

interface IVideoFormation extends mongoose.Document{
    idVideo: Number,
    idFormation: Number
}

const ViForModel = new mongoose.Schema({
    idVideo: {
        type:  Number,
        required: true
    },
    idFormation: {
        type: Number,
        required: true
    }
})

const VideoFormation = mongoose.model<IVideoFormation>('VideoFormation', ViForModel)

export default VideoFormation