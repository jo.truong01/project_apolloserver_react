import mongoose from 'mongoose'

interface IFormation extends mongoose.Document{
    name: string
    difficulty: Number
    price: Number
    description: string
}

const FormationModel = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    difficulty: {
        type: Number,
        required: true
    },
    price: {
        type:Number,
        required: true
    },
    description: {
        type: String,
        required: false
    }
})

const Formation = mongoose.model<IFormation>('Formation', FormationModel)

export default Formation