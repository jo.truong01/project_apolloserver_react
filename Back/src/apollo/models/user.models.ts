import mongoose from 'mongoose'
interface IUser extends mongoose.Document{
    firstName: string,
    lastName: string,
    email: string,
    phone: string,
    pwd: string,
    role: number
}

const UserModel = new mongoose.Schema({
    firstName: {
        type: String,
        required: true
    },
    lastName : {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true
    },
    pwd: {
        type: String,
        required: true
    },
    role: {
        // 1 normal 2 modo 3 admin 0 banni
        default: 1,
        type: Number,
        required: false,
    }
})

const User = mongoose.model<IUser>('User', UserModel)

export default User