// import all schemas
import userSchema from './user.schema'
import videoSchema from './video.schema'
import formationSchema from './formation.schema'
import videoFormationSchema from './videoFormation.schema'


export default [userSchema, videoSchema, formationSchema, videoFormationSchema]