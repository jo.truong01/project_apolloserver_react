import {gql} from 'apollo-server-express'

export default gql`
    type Video {
        id: ID!
        name: String!
        path: String!
    }
    extend type Query {
        getAllVideo: [Video]
        videoTest: String!
    }
    extend type Mutation {
        createVideo(name:String!, path:String!):Video
    }
` 