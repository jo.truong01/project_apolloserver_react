import {gql} from 'apollo-server-express'

export default gql`
    type VideoFormation {
        idFormation: Int!
        idVideo: Int!
    }
    extend type Query {
        getVideoFormation: VideoFormation!
        videoFormationTest: String!
    }
    extend type Mutation {
        createVideoFormation(idFormation:Int!, idVideo:Int!): VideoFormation!
    }
`