import {gql} from 'apollo-server-express'

export default gql`
    type User {
        id: ID!
        lastName: String!
        firstName: String!
        email: String!
        phone: String!
        pwd: String!
        role: Int!
    }
    type Query {
        getUser: User!
        hello: String!
        getAllUser: [User!]
        LogUser: User!
    }
    type Mutation {
        createUser(lastName: String!, firstName: String!, email: String!, phone: String!, pwd: String!,role: Int!):User
    }
` 