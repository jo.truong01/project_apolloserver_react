import {gql} from 'apollo-server-express'

export default gql`
    type Formation {
        id: ID!
        name: String!
        description: String!
        price: String!
        difficulty: String!
    }
    input FormationInput {
        id: ID!
        name: String!
        description: String!
        price: String!
        difficulty: String!
    }
    extend type Query {
        getAllFormation: [Formation]
        formationTest: String!
        getFormation: Formation
    }
    extend type Mutation {
        createFormation(name: String!, description: String!, price: Int!, difficulty: Int!):Formation
        modifyFormation(id: String!, modif: FormationInput): Formation
    }
` 