import Video from '../models/video.models'

export default {
    Query: {
        videoTest:() => "video resolver works",
        getAllVideo:() =>{
            try{
                return Video.find()
            }
            catch (error) {
                throw new Error(error)
            }
        }
    },
    Mutation: {
        createVideo:async (parents: any, args: any) => {
            console.log('ARGS =>', args)
            try {
                return await Video.create({
                    name: args.name,
                    path: args.path
                })
            }
            catch(err) {
                throw new Error(err)
            }
        }
    }
}