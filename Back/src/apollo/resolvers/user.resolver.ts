import User from '../models/user.models'

export default {
    Query: {
        hello:() => "Salut",
        getUser:async(id:any) =>{
            try{
                return await User.findById({_id:id})
            }
            catch (error) {
                throw new Error(error)
            }
        },
        getAllUser:async()=>{
            try {
                return await User.find()
            }
            catch(err){
                throw new Error(err)
            }
        },
        LogUser:async(parents:any ,args: any) => {
            try{
                return await User.find({email: args.email, pwd: args.pwd})
            }
            catch(err) {
                throw new Error(err)
            }
        }
    },
    Mutation: {
        createUser:async (parents: any, args: any) => {

            try {
                return await User.create({
                    firstName: args.firstName,
                    lastName: args.lastName,
                    email: args.email,
                    phone: args.phone,
                    pwd: args.pwd,
                    role: args.role
                })
            }
            catch(err) {
                throw new Error(err)
            }
        }
    }
}