import VideoFormation from '../models/videoFormation.models'

export default {
    Query: {
        videoFormationTest: () => ("videoFormation OK"),
        getVideoFormation: () => {
            try {
                return VideoFormation.find()
            }
            catch (error) {
                throw new Error(error)
            }
        }
    },
    Mutation: {
        createVideoFormation: async(parents: any, args: any) => {
            try{
                return await VideoFormation.create({
                    idFormation: args.idFormation,
                    idVideo: args.idVideo
                })
            }
            catch(err) {
                throw new Error(err)   
            }
        },
    }
}