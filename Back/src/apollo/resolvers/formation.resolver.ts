import Formation from '../models/formation.models'

export default {
    Query: {
        formationTest:() => "formation resolver works",
        getAllFormation:async() =>{
            try{
                return await Formation.find()
            }
            catch (error) {
                throw new Error(error)
            }
        },
        getFormation:async(id:any) => {
            try{
                return Formation.findById({_id: id})
            }
            catch(err){
                throw new Error(err)
            }
        }
    },
    Mutation: {
        createFormation:async (args: any) => {
            try {
                return await Formation.create({
                    name: args.name, 
                    description: args.description, 
                    price: args.price, 
                    difficulty: args.difficulty
                })
            }
            catch(err) {
                throw new Error(err)
            }
        },
        modifyFormation:async (parents: any, args: any) => {
            try{
                return await Formation.findOneAndUpdate({_id: parents}, args)
            }
            catch(err){
                throw new Error(err)
            }
        }
    }
}