// import all resolver
import userResovler from './user.resolver'
import videoResolver from './video.resolver'
import formationResolver from './formation.resolver'
import videoFormationResolver from './videoFormation.resolver'

export default [userResovler, videoResolver, formationResolver, videoFormationResolver ]