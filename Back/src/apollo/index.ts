import {ApolloServer} from 'apollo-server-express'
import typeDefs from '../apollo/schemas'
import resolvers from '../apollo/resolvers'

export const apolloServer = new ApolloServer({
    typeDefs: typeDefs,
    resolvers
})